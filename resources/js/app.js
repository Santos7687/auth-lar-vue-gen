/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('dashboard-component', require('./components/admin/DashboardComponent.vue').default);
// Vue.component('page-title-component', require('./components/admin/partials/PageTitleComponent.vue').default);
Vue.component('user-component', require('./components/admin/user/IndexUser.vue').default);
Vue.component('role-component', require('./components/admin/role/IndexRole.vue').default);
Vue.component('permission-component', require('./components/admin/permission/IndexPermission.vue').default);



// Vue.component("modal-user", require("./components/admin/user/Modal.vue").default);

// Vue.component('permission-component', require('./components/admin/PermissionComponent.vue').default);
// Vue.component('table-component', require('./components/admin/user/Table.vue').default);
//hijo de usercompoenent





/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        menu: 0,
        // permissionsa: [],
        // usera: '',

    },
    // created() {
    //     let me=this;
    //     axios
    //         .get('user/authentication')
    //         .then(function (response) {


    //             var answer =response.data ;
    //             me.permissionsa = answer.permissions;
    //             console.log("aaaaaaa");
    //             console.log(me.permissionsa);
    //             console.log("aaaaaaa");
    //             me.usera = answer.user.data;

    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //             // return error;
    //         });
    //     console.log("puerba de mixin");
    //     // return usr;
    // },


});
