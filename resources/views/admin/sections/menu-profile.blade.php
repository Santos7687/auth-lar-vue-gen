<div class="profile clearfix">
    <div class="profile_pic">
        <img src="images/img.jpg" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span>Bienbenido,</span>
        <h2>{{ Auth::user()->name }}</h2>
    </div>
    <div class="clearfix"></div>
</div>
