<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Sistema Prueba</title>

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="css/template.css" rel="stylesheet">
</head>

<body class="nav-md">
  <div id="app">
    <div class="container body">
      <div style="width: 100%; height: 100%;" class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Sicode</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            @include('admin.sections.menu-profile')

            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            @include('admin.sections.menu-sidebar')


            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            @include('admin.sections.menu-footer')

            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        @include('admin.sections.top-nav')

        <!-- /top navigation -->

        <!-- page content -->
        @yield('home')
        <!-- /page content -->

        <!-- footer content -->
        @include('admin.sections.footer')

        <!-- /footer content -->
      </div>
    </div>
  </div>

  <script src="js/app.js"></script>
  <script src="js/template.js"></script>
</body>

</html>
