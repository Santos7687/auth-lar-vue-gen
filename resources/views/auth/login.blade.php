<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/template.js') }}" defer></script>

    <link href="{{ asset('css/template.css') }}" rel="stylesheet">
    <style>
        .is-invalid {
            border-color: #e3342f;

        }
    </style>
    <!-- <script src="js/app.js"></script>
    <script src="js/template.js"></script>
    <link href="css/template.css" rel="stylesheet"> -->
</head>

<body class="login">
    <div>


        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <h1>Inicio de sesión</h1>
                        @error('username')
                        <span class="invalid-feedback text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <div>
                            <!-- <input id="email" type="email"
                                class="form-control @error('email') text-danger @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email"
                                autofocus> -->

                            <input id="username" name="username" type="text"
                                class="form-control @error('username') is-invalid @enderror"
                                value="{{ old('username') }}" placeholder="Username" required />

                        </div>
                        <div>
                            <!-- <input id="password" type="password"
                                class="form-control @error('password') text-danger @enderror"
                                name="password" required autocomplete="current-password"> -->
                            <input id="password" type="password" name="password"
                                class="form-control @error('password') is-invalid @enderror" placeholder="Password"
                                required />

                            @error('password')
                            <span class="invalid-feedback  " role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div>
                            <button class="btn btn-default " type="submit">Ingresar</button>
                            <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1><i class="fa fa-eye"></i> Santos7687!</h1>
                                <p>©2020</p>
                                <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and
                                    Terms</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>

        </div>
    </div>
</body>

</html>
