<?php

namespace App\UserAuthorization\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'name', 'slug', 'description','state'
    ];
    public function roles(){
        return $this->belongsToMany('App\UserAuthorization\Models\Role')->withTimestamps();
    }
}
 