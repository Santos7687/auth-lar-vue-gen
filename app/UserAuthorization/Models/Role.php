<?php

namespace App\UserAuthorization\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'slug', 'description','full_access','state'
    ];

    public function users(){
        return $this->belongsToMany('App\User')->withTimestamps();
    }
    public function permissions(){
        return $this->belongsToMany('App\UserAuthorization\Models\Permission')->withTimestamps();
    }
}
