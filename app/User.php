<?php

namespace App;

use App\UserAuthorization\Models\Permission;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\UserAuthorization\Traits\UserTrait;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable, UserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'identity_card', 'expended', 'phone', 'email', 'username', 'password', 'state',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function getAllPermissionsAttribute()
    // {
    //     $permissions = [];
    //     foreach (Permission::all() as $permission) {
    //         if (Auth::user()->can($permission->name)) {
    //             $permissions[] = $permission->name;
    //         }
    //     }
    //     return $permissions;
    // }
}
