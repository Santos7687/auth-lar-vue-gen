<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // protected function authenticated(Request $request, $user)
    // {
    //     $credentials = array('username' => $request->username, 'password' => $request->password, "state" => 1);
    //     if (Auth::attempt($credentials, false)) {

    //         return redirect()->intended('/');
    //     } else {
    //         //when echoing something here it is always displayed thus admin login is just refreshed.
    //         return redirect('/login')->withInput()->with('message', 'Login Failed o cuenta desactivada');
    //     }
    // }

}
