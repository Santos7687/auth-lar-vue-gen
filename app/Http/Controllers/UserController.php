<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAuthorization\Models\Role;
use App\UserAuthorization\Models\Permission;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Gate::authorize('haveaccess','user.index');
        $search = $request->search;
        $criterion = $request->criterion;
        $users = User::where('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.last_name', 'like', '%' . $search . '%')
            ->orWhere('users.identity_card', 'like', '%' . $search . '%')
            ->orWhere('users.username', 'like', '%' . $search . '%')

            ->with('roles')
            // join('role_user','users.id','=','role_user.user_id')
            // ->join('roles','role_user.roles_id','=','roles.id')
            // ->select('users.name','users.email','roles.name as role','roles.description')
            ->orderBy('users.id', 'Desc')->paginate(7);

        $user = Auth::user();
        // $user=User::find(Auth::user()->id);
        // $permissions=$user->getAllPermissionsAttribute();
        return [
            'pagination' => [
                'total'          => $users->total(),
                'current_page'   => $users->currentPage(),
                'per_page'       => $users->perPage(),
                'last_page'      => $users->lastPage(),
                'from'           => $users->firstItem(),
                'to'             => $users->lastItem(),
            ],
            'users' => $users,
            'user' => $user

        ];
        // return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('create',User::class);
        // return 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required|max:50',
            'last_name'     => 'required|',
            'identity_card' => 'required|alpha_dash|min:5|unique:users,identity_card',
            'username'      => 'required|max:50|unique:users,username',
            'phone'         => 'numeric|min:5|nullable',
            'email'         => 'required|max:50|email',
            'expended'      => 'required',
            'username'      => 'required|min:5',
            'password'      => 'required|min:5',
            'roles_id'       => 'required',
        ]);

        $user = User::create($request->all());

        // $role->permissions()->sync($request->get('permissions_id'));

        $user->roles()->sync($request->get('roles_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // $this->authorize('view', [$user, ['user.show','userown.show'] ]);
        $roles = Role::orderBy('name')->get();

        //return $roles;

        return view('user.view', compact('roles', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('view', $user);
        // $this->authorize('update', [$user, ['user.edit','userown.edit'] ]);
        $roles = Role::orderBy('name')->get();

        //return $roles;

        return view('user.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'          => 'required|max:50|',
            'last_name'     => 'required',
            'identity_card' => 'required|alpha_dash|min:5|unique:users,identity_card,' . $request->get('id'),
            'username'      => 'required|min:5|max:50|unique:users,username,' . $request->get('id'),
            'phone'         => 'numeric|min:5|nullable',
            'email'         => 'required|max:50|email',
            'expended'      => 'required',
            'password'      => 'min:5',
            'roles_id'       => 'required',
            'id'            => 'required|numeric'
        ]);
        $user = User::find($request->get('id'));


        $user->identity_card = $request->get('identity_card');
        $user->expended = $request->get('expended');
        $user->name = $request->get('name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->username = $request->get('username');
        $user->phone = $request->get('phone');

        if ($request->get('reset_password')) {
            $user->password = Hash::make('12345678');
        } elseif ($request->get('password') != null) {
            $user->password = Hash::make($request->get('password'));
        }
        $user->state = 1;
        $user->save();

        $user->roles()->sync($request->get('roles_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // $this->authorize('haveaccess','user.destroy');
        $user->delete();

        return redirect()->route('user.index')
            ->with('status_success', 'User successfully removed');
    }

    public function activate(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::find($request->id);
        $user->state = 1;
        $user->save();
    }
    public function deactivate(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::find($request->id);
        $user->state = 0;
        $user->save();
    }

    public function authentication(Request $request)
    {

        //     $user=User::where('users.id',Auth::user()->id)->with('roles')->get();

        //    $permissions=User::where('users.id',Auth::user()->id)
        // //    ->with('roles')->get();
        //    ->join('role_user','users.id','=','role_user.user_id')
        //    ->join('roles','role_user.role_id','=','roles.id')
        //    ->join('permission_role','role_user.role_id','=','permission_role.role_id')
        //    ->join('permissions','permission_role.permission_id','=','permissions.id')
        //    ->select('users.id as user_id','role_user.role_id','permissions.id', 'permissions.slug','roles.full_access')
        //    ->get();
        //     return ['permissions'=>$permissions, 'user'=>$user];
        $create = User::find(Auth::user()->id)->can('haveaccess',$request->create);
        $show = User::find(Auth::user()->id)->can('haveaccess',$request->show);
        $update = User::find(Auth::user()->id)->can('haveaccess', $request->update);
        $active = User::find(Auth::user()->id)->can('haveaccess', $request->active);
        $desactive = User::find(Auth::user()->id)->can('haveaccess', $request->desactive);



        return [
            'create' => $create,
            'show' => $show,
            'update' => $update,
            'active' => $active,
            'desactive' => $desactive,
            // 'elshow' => $request->show,

        ];
    }
}
