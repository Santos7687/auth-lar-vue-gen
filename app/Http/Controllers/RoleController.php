<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAuthorization\Models\Role;
use App\UserAuthorization\Models\Permission;
use Illuminate\Support\Facades\Gate;

use function Psy\debug;

class RoleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Gate::authorize('haveaccess','role.index');

        // $roles =  Role::orderBy('id','Desc')->paginate(2);

        // return view('role.index',compact('roles'));

        // Gate::authorize('haveaccess','user.index');
        $search = $request->search;
        $criterion = $request->criterion;
        $roles = Role::where('roles.name', 'like', '%' . $search . '%')
            ->orWhere('roles.slug', 'like', '%' . $search . '%')
            ->orWhere('roles.description', 'like', '%' . $search . '%')
            ->with('permissions')
            ->orderBy('roles.created_at', 'desc')->paginate(7);
        return [
            'pagination' => [
                'total'          => $roles->total(),
                'current_page'   => $roles->currentPage(),
                'per_page'       => $roles->perPage(),
                'last_page'      => $roles->lastPage(),
                'from'           => $roles->firstItem(),
                'to'             => $roles->lastItem(),
            ],
            'roles' => $roles
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('haveaccess', 'role.create');

        $permissions = Permission::get();

        return view('role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Gate::authorize('haveaccess', 'role.create');

        $request->validate([
            'name'          => 'required|max:50|unique:roles,name',
            'slug'          => 'required|max:50|unique:roles,slug',
            'full_access'   => 'required'
        ]);

        $role = Role::create($request->all());


        // $per_sel=[];
        // foreach ($permissions_select as $ps) {
        //     // $per_sel.push() =$ps->id
        // }
        //if ($request->get('permission')) {
        //return $request->all();
        $role->permissions()->sync($request->get('permissions_id'));
        //}
        // return redirect()->route('role.index')
        //     ->with('status_success', 'Role saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $this->authorize('haveaccess', 'role.show');

        $permission_role = [];

        foreach ($role->permissions as $permission) {
            $permission_role[] = $permission->id;
        }
        //return   $permission_role;


        //return $role;
        $permissions = Permission::get();




        return view('role.view', compact('permissions', 'role', 'permission_role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {

        $this->authorize('haveaccess', 'role.edit');
        $permission_role = [];

        foreach ($role->permissions as $permission) {
            $permission_role[] = $permission->id;
        }
        //return   $permission_role;


        //return $role;
        $permissions = Permission::get();




        return view('role.edit', compact('permissions', 'role', 'permission_role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // $this->authorize('haveaccess', 'role.edit');
        $request->validate([
            'name'          => 'required|max:50|unique:roles,name,' . $request->id,
            'slug'          => 'required|max:50|unique:roles,slug,' . $request->id,
            'full_access'   => 'required'
        ]);
        $role = Role::find($request->id);
        $role->update($request->all());
        $role->permissions()->sync($request->get('permissions_id'));

        //if ($request->get('permission')) {
        //return $request->all();
        // $role->permissions()->sync($request->get('permission'));
        // //}
        // return redirect()->route('role.index')
        //     ->with('status_success', 'Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('haveaccess', 'role.destroy');
        $role->delete();

        return redirect()->route('role.index')
            ->with('status_success', 'Role successfully removed');
    }

    public function getRoles(Request $request)
    {

        // $operator=$request->criterion==="yes"?"=":"!=";
        $roles = Role::orderBy('name', 'Desc')->get();
        return ["roles" => $roles];
    }

    public function activate(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Role::find($request->id);
        $user->state = 1;
        $user->save();
    }
    public function deactivate(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Role::find($request->id);
        $user->state = 0;
        $user->save();
    }
}
