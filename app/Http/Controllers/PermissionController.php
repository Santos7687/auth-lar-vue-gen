<?php

namespace App\Http\Controllers;

use App\UserAuthorization\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $criterion = $request->criterion;
        $permissions = Permission::where('name', 'like', '%' . $search . '%')
            ->orWhere('slug', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->orderBy('created_at', 'desc')->paginate(10);
        return [
            'pagination' => [
                'total'          => $permissions->total(),
                'current_page'   => $permissions->currentPage(),
                'per_page'       => $permissions->perPage(),
                'last_page'      => $permissions->lastPage(),
                'from'           => $permissions->firstItem(),
                'to'             => $permissions->lastItem(),
            ],
            'permissions' => $permissions
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required|max:50|unique:permissions,name',
            'slug'          => 'required|max:50|unique:permissions,slug',
        ]);
        Permission::create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'          => 'required|max:50|unique:permissions,name,' . $request->id,
            'slug'          => 'required|max:50|unique:permissions,slug,' . $request->id,
        ]);
        $permission = Permission::find($request->id);
        $permission->update($request->all());

        // $permission->name = $request->get('name');
        // $permission->slug = $request->get('slug');
        // $permission->description = $request->get('description');
        // $permission->state=1;
        // $permission->save();

        //
    }

    //lista los permisos que estana activos para la vista permissions
    public function getPermissions()
    {
        $permissions = Permission::where('state', 1)->orderBy('name', 'Asc')->get();
        return ["permissions" => $permissions];
    }

    public function activate(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $permission = Permission::find($request->id);
        $permission->state = 1;
        $permission->save();
    }
    public function deactivate(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $permission = Permission::find($request->id);
        $permission->state = 0;
        $permission->save();
    }
}
