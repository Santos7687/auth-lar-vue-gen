const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'resources/template/bootstrap/dist/css/bootstrap.min.css',
    'resources/template/font-awesome/css/font-awesome.min.css',
    'resources/template/nprogress/nprogress.css',
    'resources/template/tables-css-js/iCheck/skins/flat/green.css',
    'resources/template/animate.css/animate.min.css',

    'resources/template/build/css/custom.min.css',

], 'public/css/template.css')
    .scripts([
        'resources/template/jquery/dist/jquery.min.js',
        'resources/template/bootstrap/dist/js/bootstrap.min.js',
        'resources/template/fastclick/lib/fastclick.js',
        'resources/template/nprogress/nprogress.js',

        'resources/template/iCheck/icheck.min.js',

        'resources/template/build/js/custom.js',

    ], 'public/js/template.js').js(['resources/js/app.js'], 'public/js/app.js')
    .js('node_modules/popper.js/dist/popper.js', 'public/js').sourceMaps();

    // .js('resources/template/tables-css-js/pdfmake/build/pdfmake.min.js', 'public/js').sourceMaps()





