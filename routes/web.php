<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\UserAuthorization\Models\Role;
use App\UserAuthorization\Models\Permission;
use Illuminate\Support\Facades\Gate;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/hola', function () {
    return view('admin/layouts/dashboard');
});
Route::get('/prueba', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('welcome');
});




// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index');

// Route::post('/logout', 'LoginController@logout')->name('logout');

Route::get('/role', 'RoleController@index')->name('role.index');
Route::post('/role/store', 'RoleController@store')->name('role.store');
Route::put('/role/update', 'RoleController@update')->name('role.store');
Route::put('/role/activate', 'RoleController@activate')->name('role.activate');
Route::put('/role/deactivate', 'RoleController@deactivate')->name('role.deactivate');
Route::get('/get/roles', 'RoleController@getRoles')->name('getRoles');

Route::get('/permission', 'PermissionController@index')->name('permission.index');
Route::post('/permission/store', 'PermissionController@store')->name('permission.store');
Route::put('/permission/update', 'PermissionController@update')->name('permission.update');
Route::put('/permission/activate', 'PermissionController@activate')->name('permission.activate');
Route::put('/permission/deactivate', 'PermissionController@deactivate')->name('permission.deactivate');
Route::get('/get/permissions', 'PermissionController@getPermissions')->name('getPermissions');

// Route::resource('/role', 'RoleController')->names('role');

// Route::resource('/user', 'UserController')->names('user');
Route::get('/user', 'UserController@index')->name('user.index');
Route::get('/user/authentication', 'UserController@authentication')->name('user.authentication');

Route::post('/user/store', 'UserController@store')->name('user.store');
Route::put('/user/update', 'UserController@update')->name('user.update');
Route::put('/user/activate', 'UserController@activate')->name('user.activate');
Route::put('/user/deactivate', 'UserController@deactivate')->name('user.deactivate');




Route::get('test', function () {
    // return Roled::get();
    // $user=User::find(1);
    // Gate::authorize('haveaccess','user.show');
    // return $user;

    //     return $user->havePermission('user.create');
    $permissions = Permission::where('state', 1)->orderBy('name', 'Asc')->get()->groupBy('table');
    $all = [];
    foreach ($permissions as $p) {
       $all[] = $p;
    }

    return ["all" => $permissions];
});
