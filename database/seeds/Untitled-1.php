 $permission = Permission::create([
 'name' => 'List user',
 'slug' => 'user.index',
 'description' => 'A user can list user',
 'state' => '1'
 ]);

 $permission_all[] = $permission->id;

 $permission = Permission::create([
 'name' => 'Show user',
 'slug' => 'user.show',
 'description' => 'A user can show user',
 'state' => '1'
 ]);

 $permission_all[] = $permission->id;

 $permission = Permission::create([
 'name' => 'Create user',
 'slug' => 'user.create',
 'description' => 'A user can Create user',
 'state' => '1'
 ]);

 $permission_all[] = $permission->id;

 $permission = Permission::create([
 'name' => 'Edit user',
 'slug' => 'user.edit',
 'description' => 'A user can Edit user',
 'state' => '1'
 ]);

 $permission_all[] = $permission->id;

 $permission = Permission::create([
 'name' => 'Destroy user',
 'slug' => 'user.destroy',
 'description' => 'A user can Destroy user',
 'state' => '1'
 ]);
 $permission_all[] = $permission->id;