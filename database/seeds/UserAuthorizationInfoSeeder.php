<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserAuthorization\Models\Role;
use App\UserAuthorization\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class UserAuthorizationInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate
        // DB::statement("SET foreign_key_checks=0");
        DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();
        Permission::truncate();
        Role::truncate();
        // DB::statement("SET foreign_key_checks=1");




        //user admin
        $useradmin = User::where('email', 'admin@admin.com')->first();
        if ($useradmin) {
            $useradmin->delete();
        }

        $useradmin = User::create([
            'name' => 'admin',
            'last_name' => 'admin',
            'identity_card' => 8023126,
            'expended' => 'CBBA',
            'email' => 'admin@admin.com',
            'username' => 'admin.admin',
            'password' => Hash::make('admin')
        ]);
        $userguest = User::create([
            'name' => 'User',
            'last_name' => 'gars',
            'identity_card' => 8023122,
            'expended' => 'CBBA',
            'email' => 'admin2@admin.com',
            'username' => 'admin2.admin2',
            'password' => Hash::make('admin'),
        ]);
        //rol admin
        $roladmin = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Administrador',
            'full_access' => 'yes',
            'state' => '1'
        ]);
        // $useradmin->roles()->sync([$roladmin->id]);

        $registreruser = Role::create([
            'name' => 'Resgistrer user',
            'slug' => 'register.user',
            'description' => 'Resgitrador de usuarios',
            'full_access' => 'no',
            'state' => '1'
        ]);
        $userguest->roles()->sync([$registreruser->id]);

        $contador = Role::create([
            'name' => 'Contador user',
            'slug' => 'contador.user',
            'description' => 'Resgitrador de usuarios',
            'full_access' => 'no',
            'state' => '1'
        ]);
        //table role_user
        $useradmin->roles()->sync([$roladmin->id]);
        //permission
        $permission_all = [];

        $permission = Permission::create([
            'name' => 'Listar Roles',
            'slug' => 'role.index',
            'description' => 'El usuario puede listar todos los usuarios',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver Rol',
            'slug' => 'role.show',
            'description' => 'El usuario puede ver informacion de los roles',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Crear Rol',
            'slug' => 'role.create',
            'description' => 'El usuario puede crear nuevos roles',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Actualizar Rol',
            'slug' => 'role.update',
            'description' => 'El usuario puede actualizar informacion de un rol',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => ' Activar Rol',
            'slug' => 'role.active',
            'description' => 'El usuario puede activar el estado de los roles',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;
        $permission = Permission::create([
            'name' => ' Desactivar Rol',
            'slug' => 'role.desactive',
            'description' => 'El usuario puede desactivar el estado de los roles',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;
        //permission users
        $permission = Permission::create([
            'name' => 'Listar Usuario',
            'slug' => 'user.index',
            'description' => 'El Usuario puede listar a todos los usuarios',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver Usuario',
            'slug' => 'user.show',
            'description' => 'EL usuario puede ver informacion de los usuarios',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Crear Usuario',
            'slug' => 'user.create',
            'description' => 'El usuario puede crear nuevos usuarios',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Actualizar Usuario',
            'slug' => 'user.update',
            'description' => 'El Usuario puede actualizar informacion de los usuarios',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Activar Usuario',
            'slug' => 'user.active',
            'description' => 'El Usuario puede activar el estado de los usuarios',
            'state' => '1'
        ]);
        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Desactivar Usuario',
            'slug' => 'user.desactive',
            'description' => 'El Usuario puede desactivar el estado de los usuarios',
            'state' => '1'
        ]);
        $permission_all[] = $permission->id;


        //permission permission=======>>>>>>>>>>>>>>
        $permission = Permission::create([
            'name' => 'Listar Permiso',
            'slug' => 'permission.index',
            'description' => 'El Usuario puede listar a todos los permisos',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Ver Permiso',
            'slug' => 'permission.show',
            'description' => 'EL usuario puede ver informacion de los permisos',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Crear Permiso',
            'slug' => 'permission.create',
            'description' => 'El usuario puede crear nuevos permisos',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Actualizar Permiso',
            'slug' => 'permission.update',
            'description' => 'El Usuario puede actualizar informacion de los permisos',
            'state' => '1'
        ]);

        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Activar Permiso',
            'slug' => 'permission.active',
            'description' => 'El Usuario puede activar el estado de los permisos',
            'state' => '1'
        ]);
        $permission_all[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Desactivar Permiso',
            'slug' => 'permission.desactive',
            'description' => 'El Usuario puede desactivar el estado de los permisos',
            'state' => '1'
        ]);
        $permission_all[] = $permission->id;
        //table permission_role
        $roladmin->permissions()->sync($permission_all);
    }
}
